db.inventory.insertMany([
	   {
	   		"name": "JavaScript for Beginners",
	   		"author": "James Doe",
	   		"price": 5000,
	   		"stocks": 50,
	   		"publisher": "JS Publishing House"
	   },
	   {
	   		"name": "HTML and CSS",
	   		"author": "John Thomas",
	   		"price": 2500,
	   		"stocks": 38,
	   		"publisher": "NY Publishers"
	   },
	   {
	   		"name": "Web Development Fundamentals",
	   		"author": "Noah Jimenez",
	   		"price": 3000,
	   		"stocks": 10,
	   		"publisher": "Big Idea Publishing House"
	   },
	   {
	   		"name": "Java Programming",
	   		"author": "David Michael",
	   		"price": 10000,
	   		"stocks": 100,
	   		"publisher": "JS Publishing House"
	   }
	]);

// Comparison Query Operators
// $gt/$gte operator

/*
	Syntax: db.collectionName.find({ field: { $gt: value }})
			db.collectionName.find({ field: { $gte: value }})
*/

db.inventory.find({
	"stocks": {
		$gt: 50
	}
})

db.inventory.find({
	"stocks": {
		$gte: 50
	}
})

// $lt/$lte operator

db.inventory.find({
	"stocks": {
		$lt: 50
	}
})

db.inventory.find({
	"stocks": {
		$lte: 50
	}
})

// $ne operator
db.inventory.find({
	"stocks": {
		$ne: 50
	}
})

// $eq operator

db.inventory.find({
	"stocks": {
		$eq: 50
	}
})

// $in operator
/*
	Syntax: db.collectionName.find({ field: { $in: [ value1, value2 ] } })
*/

db.inventory.find({
	"price": {
		$in: [10000, 5000]
	}
})

// Logical Query Operator
//$and operator
/*
	db.collectionName.find({
		$and: [
			{ fieldA: valueA },
			{ fieldB: valueB }
		]
	})
*/
db.inventory.find({
	$and: [
			{
				"stocks": { $ne:50 }
			},
			{
				"price": { $ne:5000 }
			}
		]
});

db.inventory.find({
	"stocks": { 
		$ne:50 
	},
	"price": {
	 	$ne:5000 
	}
});

db.inventory.find({
	$or: [
			{
				"name": "HTML and CSS"
			},
			{
				"name": "JS Publishing House"
			}
	]
});

db.inventory.find({
	$or: [
			{
				"author": "James Doe"
			},
			{
				"price": {
					$lte: 5000
				}
			}
	]
});

// Field Projection
/*
	Syntax: db.collectionName.find({criteria}, {field: 1})
*/

db.inventory.find(
		{
			"publisher": "JS Publishing House"
		},
		{
			"_id": 0, // --> excludes _id field from being included in search results
			"name": 1,
			"author": 1
		}
)

// Exclusion
db.inventory.find(
       {
       		"author": "Noah Jimenez"
       },
       {
			"price": 0,
			"stocks": 0
		}
)

// Inclusion and exclusion cannot be used at the same time, except if it's used for the _id field

// Evaluation Query Operator
// $regex operator
/*
	Syntax: db.collectionName.find({field: {$regex: 'pattern', $options: 'optionsValue'}})
*/

//Case sensitive query
db.inventory.find({
	"author": {
		$regex: 'M' //--> case sensitive
	}
})


//Case insensitive query
db.inventory.find({
	"author": {
		$regex: 'M',
		$options: '$i' //--> case sensitive with the $ sign
	}
})

